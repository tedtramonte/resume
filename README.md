# Theodore "Ted" Tramonte - Resume

This repository contains my resume source files. It is written in pure HTML, styled with SCSS, and bundled into one single, static `.html` file with Vite. As part of the release process, the generated resume is also converted to a `.pdf` file for maximum flexibility.

## Why?

I live and breathe Git, HTML, and CSS. When refreshing my resume one day, it dawned on me that I could achieve my design goals much faster by formatting my resume as software for the web, rather than restricting myself to using Microsoft Word.

Plus, I just think it's neat.

## Getting Started

Editing and publishing a new version of the resume is easy thanks to the simplicity of the toolchain.

### Prerequisites

- `npm`
- An OCI compliant container engine like Docker or Podman

### Usage

1. Install NPM packages

   ```sh
   npm install
   ```

2. Run the dev server and make your changes, which will refresh in your browser automatically

   ```sh
   npm run dev
   ```

3. Verify output locally before pushing

    ```bash
    # Produces the "statically compiled" HTML
    npm run build
    # Takes that HTML and "prints to PDF" using Chrome
    docker run -v $(pwd)/public:/workspace pink33n/html-to-pdf --url http://localhost/index.html --pdf Theodore_Ted_Tramonte_Resume.pdf --no-margins --scale .76
    ```

If it all looks good, push the commit and let GitLab CI/CD do the rest!

## Contributing

I'm not sure why you'd want to contribute to _my_ resume, but in the spirit of FOSS: merge requests are welcome after opening an issue first.

## Acknowledgments

- [richardtallent/vite-plugin-singlefile](https://github.com/richardtallent/vite-plugin-singlefile) for brilliantly allowing Vite to inline static assets
- [vbenjs/vite-plugin-html](https://github.com/vbenjs/vite-plugin-html) for safely minifying the HTML
- [pinkeen/docker-html-to-pdf](https://github.com/pinkeen/docker-html-to-pdf) for slickly devising a way to use headless Chrome to create a PDF
