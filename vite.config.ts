import path from 'path';
import { defineConfig } from "vite";
import { createHtmlPlugin } from 'vite-plugin-html';
import { viteSingleFile } from "vite-plugin-singlefile";

export default defineConfig({
    root: path.resolve(__dirname, 'src'),
    plugins: [
        viteSingleFile(),
        createHtmlPlugin({
            minify: true,
        }),
    ],
    build: {
        outDir: path.resolve(__dirname, 'public')
    }
});
